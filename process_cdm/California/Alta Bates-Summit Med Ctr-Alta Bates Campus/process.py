import os
import sys
import pandas
sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))

folder = os.path.basename(here)
basePath=here.split("process_cdm")[0]
destination=os.path.join(basePath,"CDM")
if not os.path.exists(destination):
    os.mkdir(destination)
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'California'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)

state = os.path.join(destination, "California")
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
if(os.path.exists(destination)):
    os.remove(destination)    #Removing from CDM

for filename in os.listdir(here):
    name = folder
    filename = os.path.join(here, filename)

    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    xls = pandas.ExcelFile(filename)
    for sheet in xls.sheet_names:
        df = pandas.read_excel(filename, sheet)
        if 'drg' in sheet.lower():
            n=0
            category="DRG"
            charge=""
            description=""
            while(len(charge)==0 or len(description)==0):
                n+=1
                df = pandas.read_excel(filename, sheet,skiprows=n)
                for col in df.columns:
                    if 'DRG Title' in col:
                        description=col
                    elif 'Avg Billed Charges' in col:
                        charge=col
            a=ParseData()
            a.ProcessXLSX(n, filename,  name, charge, description, category,destination,sheet)


        elif 'cdm alta' in sheet.lower():
            n=0
            df = pandas.read_excel(filename, sheet,skiprows=n)
            category="Standard"
            for col in df.columns:
                if 'Billing Desc' in col:
                    description=col
                elif 'Current Inpatient Price' in col:
                    charge=col
            a = ParseData()
            a.ProcessXLSX(n, filename,  name, charge, description, category, destination, sheet)

        elif 'rx' in sheet.lower():
            n=0
            df = pandas.read_excel(filename, sheet,skiprows=n)
            category="Pharmacy"
            for col in df.columns:
                if 'Drug Name' in col:
                    description=col
                elif 'Calculated Price' in col:
                    charge=col
            a = ParseData()
            a.ProcessXLSX(n, filename,  name, charge, description, category, destination, sheet)



