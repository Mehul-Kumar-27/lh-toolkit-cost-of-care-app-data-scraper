urls = [
    #Alaska
    ['https://alaska.providence.org/locations/p/pamc/patients/pricing-transparency', '', "Providence Alaska Medical Center", "Alaska"],
      ['https://www.bartletthospital.org/patients-visitors/estimate-of-charges/', 'https://www.bartletthospital.org', "Bartlett Regional Hospital", "Alaska"],
      ['https://www.cpgh.org/pricing', 'https://www.cpgh.org/', "Central Peninsula Hospital", "Alaska"],
     ['https://www.cdvcmc.com/', '', "Cordova Community Medical Center", "Alaska"],
    ['https://www.orthoindy.com/hospital-pricing','https://www.orthoindy.com','Orthoindy Hospital','Alaska'],
    ['https://www.matsuregional.com/pricing-information', 'https://www.matsuregional.com/', "Mat-Su Regional Medical Center", "Alaska"],
      ['https://www.peacehealth.org/patient-financial-services/hospital-pricing', '', "PeaceHealth", "Alaska"],
      ['https://alaska.providence.org/locations/p/pamc/patients/pricing-transparency', '', "Providence Alaska Medical Center", "Alaska"],
      ['https://alaska.providence.org/locations/p/pamc/patients/pricing-transparency', '',"Providence Kodiak Island Medical Center", "Alaska"],
    ['https://alaska.providence.org/locations/p/psmcc/pricing-transparency', '',  "Providence Seward Medical Center", "Alaska"],
      [ 'https://alaska.providence.org/locations/p/pamc/patients/pricing-transparency', '', "Providence Valdez Medical Center", "Alaska"],
        ['https://arcticslope.org/services/hospital-services/ssmh-chargemaster/', '', "Samuel Simmonds Memorial Hospital", "Alaska"],
      ['http://www.sitkahospital.com/getpage.php?name=financial_services&sub=Patient+Financial+Services', 'http://www.sitkahospital.com/', "Sitka Community Hospital", "Alaska"],
      ['https://alaska.providence.org/locations/p/pamc/patients/pricing-transparency', '', "Saint Elias Specialty Hospital", "Alaska"],
    #Georgia
    ['https://www.gradyhealth.org/billing-insurance/#standard-list-charges', '', "Grady Memorial Hospital", "Georgia"],
    ['https://www.gradyhealth.org/billing-insurance/#standard-list-charges', '', "Grady General Hospital", "Georgia"],
    ['https://www.emoryhealthcare.org/patients-visitors/price-transparency.html', 'https://www.emoryhealthcare.org', "Emory HealthCare", "Georgia"],
]
