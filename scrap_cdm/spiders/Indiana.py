import requests
import scrapy
import os
import datetime
from bs4 import BeautifulSoup

urls=[
['https://www.in.gov/isdh/28202.htm', 'https://www.in.gov', "", "Indiana"]

]
today = datetime.datetime.today().strftime('%Y-%m-%d')

class IndianaSpider(scrapy.Spider):
    name = 'indiana'
    def start_requests(self):
        for url in urls:
            yield scrapy.Request(url=url[0], callback=self.parse,meta={'index':url[1],'name':url[2],'state':url[3]})


    def parse(self, response):
        soup = BeautifulSoup(response.text, 'lxml')
        for entry in soup.find_all('a', href=True):
            link = entry['href']
            base_url = response.meta['index']
            state = response.meta['state']
            link = base_url + link
            entry_name = entry.text.strip()
            self.logger.info(link)
            if '.MDB' in link or '.xlsx' in link:
                self.log("\n\n\n We got data! \n\n\n" )
                outdir = os.path.join("Data")
                if os.path.isdir(outdir) == False:
                    os.mkdir(outdir)
                path = os.path.join(outdir, 'Indiana')
                if os.path.isdir(path) == False:
                    os.mkdir(path)
                path = os.path.join(path, entry_name)
                if os.path.isdir(path) == False:
                    os.mkdir(path)
                self.log("\n\n\n We got data! \n\n\n" + path)
                self.logger.info('Saving File %s', path)
                r = requests.get(link, headers={
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36'})
                filename = link.split("/")[-1].split("?")[0]
                self.logger.info('filename')
                self.logger.info(filename)
                path = os.path.join(path, filename)
                with open(path, 'wb') as f:
                    f.write(r.content)



